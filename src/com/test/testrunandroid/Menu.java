package com.test.testrunandroid;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Menu extends ListActivity{

	String[] classes = {"Set Password","Set Gesture"};//value for list
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Intent intent = new Intent();
		intent.setClass(Menu.this, LockScreenService.class);
		startService(intent);
		setListAdapter(new ArrayAdapter<String>(Menu.this, android.R.layout.simple_list_item_1, classes));	//creates list
	}		
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		String name = classes[position];
		if(name.equals("Set Password"))
			name="PasswordSet";
		if(name.equals("Set Gesture"))
			name="AccelTest";
		try{
		Class ourClas = Class.forName("com.test.testrunandroid."+name);
		Intent ourInt = new Intent(Menu.this,ourClas);
		startActivity(ourInt);//start new activity based on list item clicked
		}
		catch(ClassNotFoundException cfn)
		{
			cfn.printStackTrace();
		}
	}


	
	

}
