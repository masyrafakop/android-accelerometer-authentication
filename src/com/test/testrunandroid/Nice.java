package com.test.testrunandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class Nice extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nice);
		//displays splash screen for 2 seconds
		Thread timer = new Thread(){
			public void run(){
				try{
					sleep(2000);
				}
				catch(InterruptedException ie)
				{
					ie.printStackTrace();
				}
				finally{
					Intent openSP = new Intent("com.test.testrunandroid.MENU");
					startActivity(openSP);
				}
			}
		};
		timer.start();
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}
	
}
