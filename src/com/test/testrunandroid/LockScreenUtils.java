package com.test.testrunandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

import com.test.testrunandroid.R;

public class LockScreenUtils {
	
	private OverlayDialog ovd;
	private OnLockStatusChangedListener olscl;
	
	public interface OnLockStatusChangedListener
	{
		public void onLockStatusChanged(boolean isLock);
	}
	
	
	public LockScreenUtils(){
		reset();
	}
	
	
	
	public void lock(Activity act){
		if(ovd==null)
		{
			ovd = new OverlayDialog(act);
			ovd.show();
			olscl = (OnLockStatusChangedListener) act;
		}
	}
	
	public void reset(){
		if(ovd !=null)
		{
			ovd.dismiss();
			ovd = null;
		}
	}
	
	public void unlock(){
		if(ovd!=null){
			ovd.dismiss();
			ovd = null;
			if(olscl!=null)
				olscl.onLockStatusChanged(false);
		}
	}
	
	private static class OverlayDialog extends AlertDialog{
		public	OverlayDialog(Activity act)
		{
			super(act,R.style.OverlayDialog);
			WindowManager.LayoutParams params = getWindow().getAttributes();
			params.type = LayoutParams.TYPE_SYSTEM_ERROR;
			params.dimAmount = 0.0F;
			params.width = 0;
			params.height = 0;
			params.gravity = Gravity.BOTTOM;
			getWindow().setAttributes(params);
			getWindow().setFlags(LayoutParams.FLAG_SHOW_WHEN_LOCKED| LayoutParams.FLAG_NOT_TOUCH_MODAL,0xffffff);
			setOwnerActivity(act);
			setCancelable(false);
		}
		
		public final boolean dispatchTouchEvent(MotionEvent motevent){
			return true;
		}
	}
}

