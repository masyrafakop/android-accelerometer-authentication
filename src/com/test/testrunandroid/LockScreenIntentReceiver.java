package com.test.testrunandroid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.test.testrunandroid.LockScreen;

public class LockScreenIntentReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		String action = intent.getAction();
		if(action.equals(Intent.ACTION_SCREEN_OFF))
		{	
			Intent i = new Intent(Constant.LOCK_SCREEN_ACTION);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
			context.startActivity(i);
		}
		else if(action.equals(Intent.ACTION_SCREEN_ON)){
			//other
		}
		else if(action.equals(Intent.ACTION_BOOT_COMPLETED)){
			//other
		}
		
	}
}
