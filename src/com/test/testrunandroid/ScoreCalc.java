package com.test.testrunandroid;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;

import android.os.Environment;

public class ScoreCalc {
	ArrayList ar = new ArrayList();
	DecimalFormat df = new DecimalFormat("0.00");
	String path = Environment.getExternalStorageDirectory()+File.separator+"Android//data//com.testrun.android";	//path for file
	File file = new File(path,"next.txt");
	
	public void someThing(String docname){
		
		
		File folder = new File(path,docname);
		StringBuilder text = new StringBuilder();
		
		double a,b,c;
		try {
		    BufferedReader br = new BufferedReader(new FileReader(folder));
		    String line;
		    
		    double[] x,y,z;
			x = new double[2];
			y = new double[2];
			z = new double[2];
			String m="";
		    if(!file.exists()){boolean first=true;
		    while ((line = br.readLine()) != null) {
		        StringTokenizer st = new StringTokenizer(line,";");
		        StringTokenizer st2 = new StringTokenizer(st.nextToken(),",");
		        a=Double.parseDouble(st2.nextToken());	//gets x-axis from file
		        b=Double.parseDouble(st2.nextToken());	//gets y-axis from file
		        c=Double.parseDouble(st2.nextToken());	//gets z-axis from file
		        
		        if(first)
				{	x[0]=a;
					y[0]=b;
					z[0]=c;
				}
				else{
					x[1]=a;
					y[1]=b;
					z[1]=c;
				}
				double[] res = new double[3];
				if(!first){
				res[0] = Math.abs(x[0]-x[1]);
				res[1] = Math.abs(y[0]-y[1]);
				res[2] = Math.abs(z[0]-z[1]);}
				
				if(!first)
				{	m = m + df.format(res[0])+","+df.format(res[1])+","+df.format(res[2])+";\n";
					x[0]=a;
					y[0]=b;
					z[0]=c;
				}
				
				if(first)
					first=false;
				
				
		    }}
		    writeToNewFile(m,file);
		    br.close();
		}	
		catch (IOException e) {
		    e.printStackTrace();
		}
		
	}
	public void writeToNewFile(String m,File f){
		
		try{
			 f.createNewFile();
			 PrintWriter out = new PrintWriter(new FileWriter(f,true));
			 out.append(m);
			 out.close();
			 
		 }catch(IOException ie)
		 {
			 ie.printStackTrace();
		 }
	}
public void someSome(Object something){
		
		ArrayList ar = (ArrayList)something;
		StringBuilder text = new StringBuilder();
		File fli = new File(path);
		fli.mkdirs();
		File fil = new File(fli,"other.txt");
		if(fil.exists())
			fil.delete();
		double a,b,c;
		
		    
		    double[] x,y,z;
			x = new double[2];
			y = new double[2];
			z = new double[2];
			boolean first=true;
			for(int xx=0;xx<ar.size();xx++){
		        StringTokenizer st = new StringTokenizer((String)ar.get(xx),";");
		        StringTokenizer st2 = new StringTokenizer(st.nextToken(),",");
		        a=Double.parseDouble(st2.nextToken());
		        b=Double.parseDouble(st2.nextToken());
		        c=Double.parseDouble(st2.nextToken());
		        
		        if(first)
				{	x[0]=a;
					y[0]=b;
					z[0]=c;
				}
				else{
					x[1]=a;
					y[1]=b;
					z[1]=c;
				}
				double[] res = new double[3];
				res[0] = Math.abs(x[0]-x[1]);
				res[1] = Math.abs(y[0]-y[1]);
				res[2] = Math.abs(z[0]-z[1]);
				String m="";
				if(!first)
				{	m = m+df.format(res[0])+","+df.format(res[1])+","+df.format(res[2])+";\n";
					x[0]=a;
					y[0]=b;
					z[0]=c;
				}
				if(first)
					first=false;
				
				try{
					 fil.createNewFile();
					 PrintWriter out = new PrintWriter(new FileWriter(fil,true));
					 out.append(m);
					 out.close();
					 
				 }catch(IOException ie)
				 {
					 ie.printStackTrace();
				 }
		    }
		    
		  
		}

	public int score()
	{
		
		File folder = new File(path,"next.txt");
		File fil = new File(path,"other.txt");
		String val;
		ArrayList file1 = new ArrayList();
		ArrayList file2 = new ArrayList();
		String val2=null;
		double amax=0,bmax=0;
		double a1=0.0,a2=0.0,a3=0.0,b1=0.0,b2=0.0,b3=0.0;
		int c=0;
		try{
			BufferedReader br = new BufferedReader(new FileReader(folder));
			BufferedReader br2 = new BufferedReader(new FileReader(fil));
			while((val = br.readLine()) !=null){
				StringTokenizer st = new StringTokenizer(val,";");
				file1.add(st.nextToken());
				}
			while((val2=br2.readLine())!=null){
				StringTokenizer st = new StringTokenizer(val2,";");
				file2.add(st.nextToken());
			}
			int size = Math.max(file1.size(),file2.size());
			if(file2.size()<file1.size()-3||file2.size()>file1.size()+3)
				c=c+500;
			for(int x=0;x<size;x++)
			{
				if(x<file1.size()-1)
				{	StringTokenizer st = new StringTokenizer((String)file1.get(x),",");
					a1=Double.parseDouble(st.nextToken());
					a2=Double.parseDouble(st.nextToken());
					a3=Double.parseDouble(st.nextToken());
					amax=amax+Math.max(a1,Math.max(a2, a3));
				}
				else
				{	
					a1=0.0;
					a2=0.0;
					a3=0.0;
					amax=amax+Math.max(a1,Math.max(a2, a3));
				}
				if(x<file2.size()-1)
				{	
					StringTokenizer st = new StringTokenizer((String)file2.get(x),",");
					b1=Double.parseDouble(st.nextToken());
					b2=Double.parseDouble(st.nextToken());
					b3=Double.parseDouble(st.nextToken());
					bmax=bmax+Math.max(b1,Math.max(b2, b3));
				}
				else
				{	StringTokenizer st = new StringTokenizer((String)file2.get(file2.size()-1),",");
					b1=Double.parseDouble(st.nextToken());
					b2=Double.parseDouble(st.nextToken());
					b3=Double.parseDouble(st.nextToken());
					bmax=bmax+Math.max(b1,Math.max(b2, b3));
				}
				double f = (Math.max(Math.abs(a1-b1), Math.max(Math.abs(a2-b2), Math.abs(a3-b3))))/0.3;	//calculates 1 point of score for every 0.3m/s distance
				c = c + (int)f;
			}
			br.close();
			br2.close();
		}catch(IOException ie){
			ie.printStackTrace();
		}
		if(bmax<amax/2)
			c=c+500;
		return c;
	}
		
}
