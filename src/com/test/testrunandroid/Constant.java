package com.test.testrunandroid;

public class Constant {

	public static final String LOCK_SCREEN_ACTION = "android.intent.lockscreen";
	
	private Constant() throws InstantiationException{
		throw new InstantiationException("This class is not for instantiation");
	}
}
