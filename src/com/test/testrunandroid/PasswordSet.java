package com.test.testrunandroid;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PasswordSet extends Activity{
	String password;
	EditText input;
	EditText inpCon;
	EditText th;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.text);
		
		input = (EditText) findViewById(R.id.pass);
		input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		inpCon = (EditText) findViewById(R.id.passConfirm);
		inpCon.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		Button but = (Button) findViewById(R.id.passSet1);
		boolean get=false;
		but.setOnClickListener(new View.OnClickListener(){
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(input.getText().toString().equals(inpCon.getText().toString()))	//compares word from 1st and 2nd edittext
				{	password=input.getText().toString();
					savePass(password);	//saves password in file
					finish();	//finishes activity
				}
				else{
				Toast.makeText(PasswordSet.this, "Password did not match", Toast.LENGTH_SHORT).show();	//shows message of password did not match
				}
			}
		});
				
	}
	public void savePass(String pass){
		String path = Environment.getExternalStorageDirectory()+File.separator+"Android//data//com.testrun.android";	//path for file to be placed
		 File folder = new File(path);
		 folder.mkdirs();	//creates directory
		 File file=new File(folder,"pass.txt");
		 try{
			 file.createNewFile();
			 FileOutputStream fOut = new FileOutputStream(file);
			 OutputStreamWriter myOut = new OutputStreamWriter(fOut);
			 myOut.append(pass);	//writes data into file
			 myOut.close();	//close the file
			 
		 }catch(IOException ie)
		 {
			 ie.printStackTrace();
		 }
		 
	}

	
	

}
