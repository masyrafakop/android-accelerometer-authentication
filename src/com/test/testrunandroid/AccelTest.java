package com.test.testrunandroid;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class AccelTest extends Activity implements SensorEventListener{

	float mSensorX;
    float mSensorY;
    float mSensorZ;
    Display mDisplay;
    SensorManager sm;
    PowerManager mPowerManager;
    WindowManager mWindowManager;
    DecimalFormat df = new DecimalFormat("#.##");
    DecimalFormat df2  = new DecimalFormat("#.00");
    ScoreCalc sc = new ScoreCalc();
    TextView ax,ay,az;
    Context context;
    int z=0;
    boolean setter = false;
    Button b;
    int a=0;
    long start,end;
    int x=0;
    Timer time = new Timer();
    //float bx,by,bz;
    float cx,cy,cz;
    String record = "";
    Sensor s;
    long mid = 25;
    //TimerTask tt;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accel_test);
        // set 3 textview for display
        ax = (TextView)findViewById(R.id.accread1);
        ay = (TextView)findViewById(R.id.accread2);
        az = (TextView)findViewById(R.id.accread3);
        //button to start and stop recording motion gesture
        b = (Button)findViewById(R.id.accelactivator);
        
        b.setOnClickListener(new View.OnClickListener(){
        	
        	@Override
			public void onClick(View arg0){
        		
        		start = System.nanoTime();	//start recording time when button clicked
        		
        		if(setter==true&&a==1){
        			setter=false;
        			a++;
        			ay.setText("Recording Stopped"); //display when button is clicked to stop recording
        			sm.unregisterListener(AccelTest.this);	//unregister sensor listener for accelerometer
        			sc.someThing("test.txt");	//calls function someThing from calcScore
        			}
        		else if(setter==false&&a==0){
        			ay.setText("Recording Started");	//display when button is clicked to start recording
        			setter=true;
        			a++;
        			sm = (SensorManager) getSystemService(SENSOR_SERVICE);	//calling sensor method
        			s = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);	//initialising accelerometer
        			sm.registerListener(AccelTest.this,s,SensorManager.SENSOR_DELAY_FASTEST);	//start using accelerometer
        			
        			end = System.nanoTime();
        			
        			
        		}       		
        	}
        });
        		// Get an instance of the PowerManager
        		mPowerManager = (PowerManager) getSystemService(POWER_SERVICE);

        		// Get an instance of the WindowManager
        		mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        		
    }
    
    float bx,by,bz;
	@Override
    public void onSensorChanged(SensorEvent event){
    				 bx = event.values[0];	//records value for x-axis
    				 by = event.values[1];	//records value for y-axis
    				 bz = event.values[2];	//records value for z-axis
    				 
    				 long calc = (System.nanoTime()-start)/10000000;	//how long motion is recording
    				 
    				 if((calc==mid||calc<=mid+4&&calc>=mid-4)&&mid<=500)
    				 {record = record + df.format(bx) + "," +df.format(by) + "," + df.format(bz) +","+df.format((System.nanoTime()-start)/1000000000.00)+";\n";
    				 ax.setText(record);	//display x,y,z axis and the time of record
    				 mid+=25;
    				 }
    				 capture(record,mid);	//sends record and mid to capture method
    				 
    }
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {}
	
	public void capture(String m,long mid){
		
		double asd;
		asd = (mid-25.00)/100.00;
		 az.setText("Recorded Motion for "+asd+" seconds");
		 String path = Environment.getExternalStorageDirectory()+File.separator+"Android//data//com.testrun.android";	//path to store inside phone storage
		 File folder = new File(path);
		 folder.mkdirs();	//make directory of new file
		 
		 File file = new File(folder,"test.txt");	//create file test.txt
		 File ms = new File(folder,"next.txt");	//create file next.txt
		 ms.delete();	//delete next.txt if exist
		 try{
			 file.createNewFile();	// create new file for test.txt
			 FileOutputStream fOut = new FileOutputStream(file);
			 OutputStreamWriter myOut = new OutputStreamWriter(fOut);
			 myOut.append(m);	//insert m into the file
			 myOut.close();	//close file
			 
		 }catch(IOException ie)
		 {
			 ie.printStackTrace();
		 }
		 
	}
	
}

