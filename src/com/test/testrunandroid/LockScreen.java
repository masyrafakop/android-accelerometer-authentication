package com.test.testrunandroid;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.KeyguardManager;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.test.testrunandroid.LockScreenUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import com.test.testrunandroid.LockScreenService;

@SuppressLint("NewApi")
@SuppressWarnings("unused")
public class LockScreen extends Activity implements LockScreenUtils.OnLockStatusChangedListener{

	private Button passend;//button to unlock
	private LockScreenUtils mlockutils;		//Member variables
	private DevicePolicyManager dPM;
	EditText pass;
	String path = Environment.getExternalStorageDirectory()+File.separator+"Android//data//com.testrun.android";
	File file = new File(path,"pass.txt");
	String compass;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lock_screen);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		makeFullScreen();	//makes activity full screen
		startService(new Intent(this,LockScreenService.class));
		
		
		passend = (Button) findViewById(R.id.btnUnlock);
		pass = (EditText) findViewById(R.id.passUn);
		pass.setInputType(InputType.TYPE_CLASS_TEXT| InputType.TYPE_TEXT_VARIATION_PASSWORD);//set input type to text and password type
		try{
			BufferedReader br = new BufferedReader(new FileReader(file));	//read from password file
			compass = br.readLine();
		}catch(Exception e){
			e.printStackTrace();
		}
		passend.setOnClickListener(new View.OnClickListener() {	
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//unlockScreen(v);
				if(pass.getText().toString().equals(compass))	//compares current password with the setted password
					finish();	//finish activity
			}
		});
		
		
	}
	
	//makes activity full screen
	@SuppressLint("NewApi")
	public void makeFullScreen(){
		KeyguardManager myKeyManager = (KeyguardManager)getSystemService(Context.KEYGUARD_SERVICE);
		if( myKeyManager.inKeyguardRestrictedInputMode())
	        return;
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
		this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
		//makeFullScreen();
		if(Build.VERSION.SDK_INT < 19){
			this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		}
		else{
			this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE);
		}
	}
	//action triggered when back button pressed
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		return;
	}
	
	//action triggered when physical button pressed
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) || (keyCode == KeyEvent.KEYCODE_POWER)
				|| (keyCode == KeyEvent.KEYCODE_VOLUME_UP) || (keyCode == KeyEvent.KEYCODE_CAMERA)){
			return true;
		}
		if(keyCode == KeyEvent.KEYCODE_HOME)
			return true;
		
		return false;
	}


	public void lockHomeButton(){
		mlockutils.lock(LockScreen.this);
	}
	public void unlockHomeButton(){
		mlockutils.unlock();
	}
	
	@Override
	public void onLockStatusChanged(boolean isLock) {
		// TODO Auto-generated method stub
		if(!isLock)
			unlockDevice();
	}
	private void unlockDevice()
	{
		finish();
	}
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP
				|| (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN)
				|| (event.getKeyCode() == KeyEvent.KEYCODE_POWER)) {
			return false;
		}
		if ((event.getKeyCode() == KeyEvent.KEYCODE_HOME)) {

			return true;
		}
		return false;
}

}
