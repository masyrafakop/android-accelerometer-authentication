package com.test.testrunandroid;

import java.util.List;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;

public class LaunchHome extends Activity{

	private String mPackageName;
	private String mClassName;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.launch_home);
	}

	
	public static boolean isEffective(String str){
		
		if((str==null)||("".equals(str))||(" ".equals(str))||("null".equals(str))||("\n".equals(str)))
			return false;
		else
			return true;
	}
	private void getLauncherPackName(Context context){
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		
		List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.GET_ACTIVITIES);
		if(resInfoList !=null)
		{
			ResolveInfo resInfo;
			for (int i = 0; i < resInfoList.size(); i++) {
                resInfo = resInfoList.get(i);
                if ((resInfo.activityInfo.applicationInfo.flags &
                        ApplicationInfo.FLAG_SYSTEM) > 0) {
                    mPackageName = resInfo.activityInfo.packageName;
                    mClassName = resInfo.activityInfo.name;

                    break;
                }
			}
		}
	}
	
	
	

}
