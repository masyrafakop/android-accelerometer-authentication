package com.test.testrunandroid;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class GyroTest extends Activity implements SensorEventListener{

	TextView gr;
	SensorManager sm;
	Sensor sr;
	String record="";
	boolean start = false;
	boolean exe=false;
	Button st;
	long sta,end;
	ScoreCalc sc = new ScoreCalc();
	int thresh=50;
	int g;
	ArrayList ar = new ArrayList();
	DecimalFormat df = new DecimalFormat("##.##");
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.gyro_test);
		gr=(TextView)findViewById(R.id.gyroRead1);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);	//disables keyguard
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		thresh=calcThresh();	//calls calcThresh method
		makeFullScreen();	//makes activity full screen
		st=(Button)findViewById(R.id.gyroButton1);
		startService(new Intent(this,LockScreenService.class));
		st.setOnClickListener(new View.OnClickListener() {
			//button for recording motion gesture
			@Override
			public void onClick(View arg0) {
				sta = System.nanoTime();	//record start time
				if(start==false)
				{	
					st.setText("Stop");
					gr.setText("Recording");
					sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);	//call sensor service
					sr = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);	//use accelerometer sensor
					sm.registerListener(GyroTest.this, sr, SensorManager.SENSOR_DELAY_FASTEST);	//call sensor to start listening to sensor data
					start=true;	//set flag to true
				}
				else
				{	st.setText("Start");
					start=false;
					sm.unregisterListener(GyroTest.this);	//call sensor to stop listening to sensor data
					end = System.nanoTime();	//record end time
					sc.someSome(ar);	//calls someSome method from scoreCalc class
					g=sc.score();	//calculate score for current gesture
					String path = Environment.getExternalStorageDirectory()+File.separator+"Android//data//com.testrun.android";
					 File folder = new File(path);
						File file=new File(folder,"log.txt");	//creates log file
					 //comparing score with threshold
					if(g<=thresh)
					{	String curDate = DateFormat.getDateTimeInstance().format(new Date());
						 try{
							 file.createNewFile();
							 PrintWriter out = new PrintWriter(new FileWriter(file,true));
							 out.append(curDate+" Score :"+g+" Threshold :"+thresh+"\n");	//inserts score and threshold with time in log file
							 out.close();
							 
						 }catch(IOException ie)
						 {
							 ie.printStackTrace();
						 }
						finish();	//finishes the activity
					}
					else{
						String curDate = DateFormat.getDateTimeInstance().format(new Date());
						 try{
							 file.createNewFile();
							 PrintWriter out = new PrintWriter(new FileWriter(file,true));
							 out.append(curDate+" Score :"+g+" Threshold :"+thresh);
							 out.close();
							 
						 }catch(IOException ie)
						 {
							 ie.printStackTrace();
						 }
							try {
								Class ou = Class.forName("com.test.testrunandroid.LockScreen");
								Intent it = new Intent(GyroTest.this,ou);
								startActivity(it);	//starts the activity from LockScreen class
							} catch (ClassNotFoundException e){
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							finish();
						
					}
					
						
				}
			}
		});
		
	}
	float bx,by,bz;
	long mid=25;
	public void onSensorChanged(SensorEvent event){
		
			bx= event.values[0];	//value from x-axis
			by= event.values[1];	//value from y-axis
			bz= event.values[2];	//value from z-axis
			
			long calc = (System.nanoTime()-sta)/10000000;
			
			if((calc==mid||calc<=mid+4&&calc>=mid-4)&&mid<=500)
			 {	ar.add(df.format(bx) + "," +df.format(by) + "," + df.format(bz) +","+df.format((System.nanoTime()-sta)/1000000000.00)+";\n");	//Insert data of x,y,z axis
				record = record + df.format(bx) + "," +df.format(by) + "," + df.format(bz) +","+df.format((System.nanoTime()-sta)/1000000000.00)+";\n";	//with time
			 mid+=25;	// adds 0.25 second interval before next comparison
			 }
		
	}
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}
	
	//making the activity full screen
	@SuppressLint("NewApi")
	public void makeFullScreen(){
		KeyguardManager myKeyManager = (KeyguardManager)getSystemService(Context.KEYGUARD_SERVICE);
		if( myKeyManager.inKeyguardRestrictedInputMode())
	        return;
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
		this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
		
		if(Build.VERSION.SDK_INT < 19){
			this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		}
		else{
			this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE);
		}
	}
	//calculates threshold
	public int calcThresh(){
		int th=20;
		String path = Environment.getExternalStorageDirectory()+File.separator+"Android//data//com.testrun.android";
		File folder = new File(path,"next.txt");
		String val;
		ArrayList file1=new ArrayList();
		double amax=0;
		double a1=0.0,a2=0.0,a3=0.0;
		try{
			BufferedReader br = new BufferedReader(new FileReader(folder));	//read data from next.txt
			while((val = br.readLine()) !=null){
				StringTokenizer st = new StringTokenizer(val,";");
				StringTokenizer st2 = new StringTokenizer(st.nextToken(),",");
				a1=Double.parseDouble(st2.nextToken());	//gets x axis value
				a2=Double.parseDouble(st2.nextToken());	//gets y axis value
				a3=Double.parseDouble(st2.nextToken());	//gets z axis value
				amax=amax+Math.max(a1,Math.max(a2, a3));	//adds amax with the highest value of all axis value
			}
			int q=8;
			do{
				th=th+10;	//adds 10 threshold value
				q=q+8;	//for every 8 m/s of acceleration distance
			}while(q<=amax);
			br.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return th;
	}
	
}
